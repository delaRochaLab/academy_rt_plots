# academy real time plots documentation

Package to generate daily and intersession reports and global dataframes and csvs.

## How to use it

1. Open a terminal window, activate your python environment and go to a suitable location.

2. Download the package:

    `git clone https://delaRochaLab@bitbucket.org/delaRochaLab/academy_rt_plots.git`

3. Go to the package directory.

    `cd academy_rt_pots`

4. Install it (remember to have your desired environment activated):

    `pip install -e .`
    
5. Run it:

    `academy_rt_plots`
