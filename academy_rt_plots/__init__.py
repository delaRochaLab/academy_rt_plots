try:
    from user_rt_plots import settings
except Exception:
    from defaults_and_examples.create_user import create_user_from_defaults
    create_user_from_defaults()
    from user_rt_plots import settings