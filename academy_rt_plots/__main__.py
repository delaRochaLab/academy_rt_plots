import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from matplotlib.lines import Line2D
from matplotlib.animation import FuncAnimation
from functools import reduce
import warnings
warnings.filterwarnings('ignore')

#Create settings if doesn't exist
try:
    from academy_rt_plots import settings
except Exception:
    from defaults_and_examples.create_user import create_user_from_defaults
    create_user_from_defaults()
    from user import settings

# COLORS TO PLOT
correct_first_c = 'green'
correct_other_c = 'lightgreen'
miss_c = 'black'
incorrect_c = 'orangered'
punish_c = 'firebrick'

water_c = 'teal'
lines_c = 'gray'
lines2_c = 'silver'

vg_c = 'MidnightBlue'
ds_c = 'RoyalBlue'
dm_c = 'CornflowerBlue'
dl_c = 'LightSteelBlue'

choices = 3

l_edge = 0
r_edge = 400
bins_resp = np.linspace(l_edge, r_edge, choices + 1)

######################### UTILS #########################

# CONVERT STRINGS TO LISTS
def convert_strings_to_lists(df, columns):
    """
    If the csv contains a column that is ',' separated, that column is read as a string.
    We want to convert that string to a list of values. We try to make the list float or string.
    """
    def tolist(stringvalue):
        if isinstance(stringvalue, str):
            try:
                stringvalue = stringvalue.split(sep=',')
                try:
                    val = np.array(stringvalue, dtype=float)
                except:
                    val = np.array(stringvalue)
            except:
                val = np.array([])
        elif np.isnan(stringvalue):
            return np.array([])
        else:
            val = np.array([stringvalue])
        return val.tolist()

    for column in columns:
        df[column] = df[column].apply(tolist)
    return df


# COLLECT ALL REPONSES TIMES IN A COLUMN
def create_responses_time(row):
    try:
        result = row['STATE_Incorrect_START'].tolist().copy()
    except (TypeError, AttributeError):
        result = row['STATE_Incorrect_START'].copy()

    items = [row['STATE_Correct_first_START'], row['STATE_Correct_other_START'], row['STATE_Punish_START']]

    for item in items:
        if not np.isnan(item):
            result += [item]
    return result


# RESPONSE RESULT COLUMN
def create_reponse_result(row):
    result = ['incorrect'] * len(row['STATE_Incorrect_START'])
    if row['trial_result'] != 'miss' and row['trial_result'] != 'incorrect':
        result += [row['trial_result']]
    return result


# UNNESTING LISTS IN COLUMNS DATAFRAMES
def unnesting(df, explode):
    """
    Unnest columns that contain list creating a new row for each element in the list.
    The number of elements must be the same for all the columns, row by row.
    """
    length = df[explode[0]].str.len()
    idx = df.index.repeat(length)
    df1 = pd.concat([
        pd.DataFrame({x: np.concatenate(df[x].values)}) for x in explode], axis=1)
    df1.index = idx
    finaldf = df1.join(df.drop(explode, 1), how='left')
    finaldf.reset_index(drop=True, inplace=True)

    length2 = [list(range(l)) for l in length]
    length2 = [item + 1 for sublist in length2 for item in sublist]
    name = explode[0] + '_index'
    finaldf[name] = length2

    return finaldf


# COMPUTE WINDOW AVERAGE
def compute_window(data, runningwindow):
    """
    Computes a rolling average with a length of runningwindow samples.
    """
    performance = []
    for i in range(len(data)):
        if i < runningwindow:
            performance.append(round(np.mean(data[0:i + 1]), 2))
        else:
            performance.append(round(np.mean(data[i - runningwindow:i]), 2))
    return performance


# CHANCE CALCULATION
def chance_calculation(correct_th):
    screen_size = 1440 * 0.28
    chance_p = correct_th*2 / screen_size
    return chance_p


# COLUMNS SORTING FROM RAW DATA
def new_colum(column_name, trials_max, data):
    '''extract the interest column'''
    feature = 'column_name'
    sorted_df = data.loc[data['MSG'] == column_name].copy() #select rows of interest
    sorted_df.reset_index(drop=True, inplace=True)  # homogenize the index
    sorted_df.rename(columns={'VALUE': column_name}, inplace=True) #change name of the column 
    to_numeric = sorted_df[column_name].apply(pd.to_numeric, errors='coerce') # convert column to numeric if necessay
    nans = to_numeric.isnull().sum()
    if nans == 0:
        sorted_df[column_name] = to_numeric
    if sorted_df.shape[0] > trials_max: # delete duplicates
        sorted_df = sorted_df.drop_duplicates(subset='TRIAL', keep='last', inplace=False)
    return(sorted_df)

# ASSIGN COLORS
def probs_colors(prob):
    color = 'black'
    if prob == 'pvg':
        color = '#393b79'
    elif prob == 'pwm_i':
        color = '#6b6ecf'
    elif prob == 'pwm_d' or prob == 'pwm_ds':
        color = '#9c9ede'
    elif prob == 'pwm_dl':
        color = '#a55194'
    return(color)


######################### COMMON PARSING #########################

def data_parse(data):
    trials = data.loc[data['MSG'] == 'STATE_Correct_first'].copy()
    trials['trial_result'] = 'miss'
    trials['trial_output'] = 0
    trials['color'] = 'black'
    trials.loc[(trials.START > 0, 'trial_result')] = 'correct'
    trials.loc[(trials.START > 0, 'trial_output')] = 1
    trials.loc[(trials.START > 0, 'color')] = 'green'
    trials['valid_bool'] = compute_window(trials.trial_output, 20)
    trials = trials[['TRIAL', 'trial_result', 'trial_output', 'color', 'valid_bool']]
    perc_valids = trials.valid_bool.iloc[-1] * 100
    return trials, perc_valids


######################### LICKTEACHING #########################

def lickteaching_plot(trials, perc_valids, x_max):
    trials['y_val'] = 1
    labels = trials.trial_result.unique().tolist()
    colors = trials.color.unique().tolist()
    lines = [Line2D([0], [0], color=c, marker='o', markersize=7, markerfacecolor=c) for c in colors]
    custom_palette = sns.set_palette(sns.color_palette(colors))

    # Plot 1
    ax1.clear()
    sns.scatterplot(x=trials.TRIAL, y=trials.y_val, hue=trials.trial_result, palette=custom_palette, s=30,
                    ax=ax1, zorder =20)
    ax1.hlines(y=[1], xmin=0, xmax=x_max, color='gray', linestyle=':', linewidth=1, zorder=10)
    ax1.set_ylabel('')
    ax1.set_yticks([0, 1, 2])
    ax1.set_yticklabels(['', '', ''])
    ax1.set_xlabel('')
    ax1.legend(lines, labels, title='Trial result')

    # Plot2
    ax2.clear()
    sns.scatterplot(x=trials.TRIAL, y=trials.valid_bool, color='black', s=30, ax=ax2)
    sns.lineplot(x=trials.TRIAL, y=trials.valid_bool, color='black', ax=ax2)
    ax2.hlines(y=[0.5, 1], xmin=0, xmax=x_max, color='gray', linestyle=':', linewidth= 1)
    ax2.set_ylabel('Valids (%)')
    ax2.set_yticks(np.arange(0, 1.1, 0.1))
    ax2.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    ax2.set_ylim(0, 1.1)
    perc_valids_t= round(trials.trial_output.mean() * 100, 1)
    reward=trials.trial_output.sum() * 10
    label = 'Perf. last: ' + str(perc_valids) + '%' + '\n' + 'Perf total: ' + str(
        perc_valids_t) + '%' + '\n' + 'Reward: ' + str(reward) + 'ul'
    ax2.text(0.85, 0.8, label, transform=ax2.transAxes, fontsize=10, verticalalignment='top',
             bbox=dict(facecolor='white', alpha=0.5, edgecolor= lines2_c))


######################### TOUCHTEACHING #########################

def touchteaching_plot(data, trials, perc_valids, x_max):
    # Parse non-shared
    responses = data.loc[data['MSG'] == 'response_x', :]
    responses.rename(columns={'VALUE': 'response_x'}, inplace=True)
    responses['response_x'] = pd.to_numeric(responses.response_x, errors='coerce')
    responses.reset_index(drop=True, inplace=True)
    responses['response_x'] = responses['response_x'].replace(np.nan, 200)
    responses = responses[['TRIAL', 'response_x']]

    trials = pd.merge(responses, trials, on='TRIAL')


    #Plot 1
    ax1.clear()

    colors = trials.color.unique().tolist()
    custom_palette = sns.set_palette(sns.color_palette(colors))
    sns.scatterplot(x=trials.TRIAL, y=trials.response_x, hue=trials.trial_result, palette=custom_palette, s=30,  ax=ax1)
    ax1.hlines(y=[200], xmin=0, xmax=x_max, color='gray', linestyle=':', linewidth= 1)
    ax1.set_ylabel('Response (mm)')
    ax1.set_xlim(0, x_max)
    ax1.set_ylim(0, 410)

    labels = trials.trial_result.unique().tolist()
    lines = [Line2D([0], [0], color=c, marker='o', markersize=7, markerfacecolor=c) for c in colors]
    ax1.legend(lines, labels, title='Trial result')
    sns.despine()

    # Plot2
    ax2.clear()
    sns.scatterplot(x=trials.TRIAL, y=trials.valid_bool, color='black', s=30, ax=ax2)
    sns.lineplot(x=trials.TRIAL, y=trials.valid_bool, color='black', ax=ax2)
    ax2.hlines(y=[0.5, 1], xmin=0, xmax=x_max, color='gray', linestyle=':', linewidth= 1)
    ax2.set_ylabel('Valids (%)')
    ax2.set_yticks(np.arange(0, 1.1, 0.1))
    ax2.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    ax2.set_ylim(0, 1.1)

    label = 'Valids : ' + str(perc_valids) + '%'
    ax2.text(0.85, 0.8, label, transform=ax2.transAxes, fontsize=10, verticalalignment='top',
             bbox=dict(facecolor='white', alpha=0.5, edgecolor=lines2_c))

    # Plot3
    ax4.clear()

    # sns.lineplot(x=np.arange(0, mask, 1), y=hist, marker='o', markersize=6, err_style="bars", color='black', ax=ax4)

    bins_resp = np.linspace(0, 400,  choices + 1)
    hist, bins = np.histogram(trials.response_x, bins=bins_resp)
    bins_plt = bins[:-1] + (bins[1] - bins[0]) / 2
    sns.lineplot(x=bins_plt, y=hist, marker='o', markersize=8, err_style="bars", color='black', ax=ax4)

    ax4.set_xlabel('$Responses\ (r_{t})\ (mm)$')
    ax4.set_ylabel('Counts')
    ax4.set_xlim(l_edge, r_edge)



######################### STAGE TRAINING #########################

def stagetraining_plot(data, x_max):

    ###################### PARSE ######################

    var_list = ['trial_type', 'trial_result', 'stim_dur_ds', 'stim_dur_dm', 'stim_dur_dl', 'x', 'correct_th']

    #create the dict
    trials_max = len(data.TRIAL.unique())
    var_dict = {}
    for var in var_list:
        key = var
        value = new_colum(var, trials_max, data)
        var_dict[key] = value

    ### merge 1
    trials = reduce(lambda left, right: pd.merge(left, right, on='TRIAL'), var_dict.values()) #merge all the small df
    var_list.append('TRIAL')
    trials = trials[trials.columns & var_list] # keep in df only useful columns

    # Column with simplified ttypes (controls considered normals)
    trials['trial_type_simple'] = trials['trial_type']
    trials.loc[trials['trial_type'].str.contains('DS'), 'trial_type_simple']= 'DS'
    trials.loc[trials['trial_type'].str.contains('DM'), 'trial_type_simple'] = 'DM'

    # ttype colors
    trials['ttype_colors'] = vg_c
    trials.loc[trials.trial_type_simple == 'DS', 'ttype_colors'] = ds_c
    trials.loc[trials.trial_type_simple == 'DM', 'ttype_colors'] = dm_c
    trials.loc[trials.trial_type_simple == 'DL', 'ttype_colors'] = dl_c

    # trial result colors
    trials['treslt_colors'] = miss_c
    trials.loc[(trials.trial_result == 'correct_first', 'treslt_colors')] = correct_first_c
    trials.loc[(trials.trial_result == 'correct_other', 'treslt_colors')] = correct_other_c
    trials.loc[(trials.trial_result == 'incorrect', 'treslt_colors')] = incorrect_c
    trials.loc[(trials.trial_result == 'punish', 'treslt_colors')] = punish_c


    ### reponses
    responses = data.loc[data['MSG'] == 'response_x', :]
    responses.rename(columns={'VALUE': 'response_x'}, inplace=True)
    responses = convert_strings_to_lists(responses, ['response_x'])
    responses = unnesting(responses, ['response_x'])
    responses['response_x'] = pd.to_numeric(responses.response_x, errors='coerce')
    responses.reset_index(drop=True, inplace=True)
    responses = responses[['TRIAL', 'response_x', 'response_x_index']]

    ### merge 2
    dfs = [responses, trials]
    trials = reduce(lambda left, right: pd.merge(left, right, on='TRIAL'), dfs)

    # errors
    trials['error_x'] = trials['response_x'] - trials['x']

    # correct bool
    trials['correct_bool'] = np.where(trials['correct_th'] / 2 >= trials['error_x'].abs(), 1, 0)
    trials.loc[(trials.trial_result == 'miss', 'correct_bool')] = np.nan

    # first last responses selection
    first_resp_df = trials.drop_duplicates(subset='TRIAL', keep='first', inplace=False)
    last_resp_df = trials.drop_duplicates(subset='TRIAL', keep='last', inplace=False)

    # useful lists
    treslts = trials.trial_result.unique().tolist()
    treslt_colors = trials.treslt_colors.unique().tolist()
    ttypes = trials.trial_type.unique().tolist()
    ttype_colors = trials.ttype_colors.unique().tolist()
    ttypes_s = trials.trial_type_simple.unique().tolist()
    x_positions = trials.x.unique().tolist()
    x_positions.sort()

    # useful values
    stage = int(float(data.loc[data['MSG'] == 'stage'].VALUE.iloc[0]))
    substage = int(float(data.loc[data['MSG'] == 'substage'].VALUE.iloc[0]))
    correct_th = trials.correct_th.iloc[0]

    # chance calculation
    chance_p = 1/choices
    chance_lines = []
    for i in range(1, choices+1, 1):
        chance_lines.append(i*chance_p)
    chance_lines = [chance_lines[0], chance_lines[int(choices/2)], chance_lines[-1]]

    # add opto on when needed
    opto_value = 'NO OPTO CODE'
    try:
        value = new_colum('opto_on', trials_max, data)
        value = value['opto_on'].unique()
        if value[0] == 1:
            opto_value = 'OPTO ON'
        elif value[0] == 0:
            opto_value = 'OPTO OFF'
        else:
            opto_value = 'WEIRD OPTO VALUES'
    except:
        pass

    ###################### PLOTS ######################

    ### PLOT 1: ERROR

    ax1.clear()
    treslt_palette = sns.set_palette(treslt_colors, n_colors=len(treslt_colors)) # palette creation

    if data['VALUE'].str.contains('StageTraining_8B').any():
        for x in trials.x.unique():
            ax1.fill_between(trials.TRIAL, x+40, x-40, facecolor=lines2_c, alpha=0.3)
        sns.scatterplot(x=trials.TRIAL, y=trials.x,  s=10, ax=ax1, color='white', edgecolor="black")
        sns.scatterplot(x=trials.TRIAL, y=trials.response_x, hue=trials.trial_result, style=trials.trial_type_simple, s=30,
                        ax=ax1)
        ax1.set_ylabel('Touchscreen (mm)')
        ax1.set_ylim(0, 400)

    else: #ERROR PLOT
        sns.scatterplot(x=trials.TRIAL, y=trials.error_x, hue=trials.trial_result, style=trials.trial_type_simple, s=30, ax=ax1)
        ax1.hlines(y=[-correct_th/2, correct_th/2], xmin=0, xmax=x_max, color='gray', linestyle=':', linewidth=1)
        ax1.set_ylabel('Error (mm)')

    ax1.set_xlabel('')
    ax1.set_xlim(-1, x_max)

    # title informing about opto state
    ax1.set_title(opto_value, y=1.0, pad=0, fontsize=10)

    # legend
    labels = treslts + ttypes_s
    colors = treslt_colors
    for c in range(len(ttypes_s)):
        colors.append('black')
    def_markers = ['o', 'X', 's', 'P', 'D']
    markers = ['o'] * len(treslts)
    for i in range(len(ttypes_s)):
        markers.append(def_markers[i])
    lines = [
        Line2D([0], [0], color=colors[i], marker=markers[i], markersize=7, linestyle="None", markerfacecolor=colors[i])
        for i in range(len(colors))]
    ax1.legend(lines, labels, title='Trial result', loc='center', fontsize=8, bbox_to_anchor=(0.9, 0.7))



    ### PLOT 2: ACCURACY

    ax2.clear()
    ttype_palette = sns.set_palette(ttype_colors, n_colors=len(ttype_colors)) #palette creation

    # global accuracies
    ### last poke accuracy (plot only in stage 1)
    if stage == 1 and substage == 1:
        last_resp_df['acc'] = compute_window(last_resp_df.correct_bool, 20)
        acc = last_resp_df.correct_bool.mean() * 100
        label = 'Last acc: ' + str(round(acc, 2)) + '%'
        ax2.text(0.85, 0.7, label, transform=ax2.transAxes, fontsize=8, verticalalignment='top',
                 bbox=dict(facecolor='white', edgecolor=lines2_c, alpha=0.5))
    ### first poke accuracy
    first_resp_df['acc'] = compute_window(first_resp_df.correct_bool, 20)
    sns.lineplot(x=first_resp_df.TRIAL, y=first_resp_df.acc, color='black', markersize=5, ax=ax2)
    acc = first_resp_df.correct_bool.mean() * 100
    label = 'First acc: ' + str(round(acc, 2)) + '%'
    ax2.text(0.85, 0.9, label, transform=ax2.transAxes, fontsize=8, verticalalignment='top',
             bbox=dict(facecolor='white', edgecolor=lines2_c, alpha=0.5))

    # trial type accuracies
    for ttype, ttype_df in first_resp_df.groupby('trial_type_simple'):
        ttype_color = ttype_df.ttype_colors.iloc[0]
        ttype_df['acc'] = compute_window(ttype_df.correct_bool, 20)
        sns.lineplot(x=ttype_df.TRIAL, y=ttype_df.acc, ax=ax2, color=ttype_color, marker='o', markersize=5, label=ttype)

    #axis
    ax2.hlines(y=chance_lines, xmin=0, xmax=x_max, color='gray', linestyle=':', linewidth= 1)
    ax2.fill_between(first_resp_df.TRIAL, chance_lines[0], 0, facecolor=lines2_c, alpha=0.3)
    ax2.set_ylabel('Accuracy (%)')
    ax2.set_xlabel('')
    ax2.set_yticks(np.arange(0, 1.1, 0.1))
    ax2.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    ax2.set_ylim(0, 1.1)
    ax2.set_xlim(-1, x_max)
    ax2.get_legend().remove()

    #vertical lines signaling blocks
    try:
        block_size = int(data.loc[data['MSG'] == 'block_size']['VALUE'].max())
        for block in range(0, x_max, block_size):
            ax1.axvline(block, color='gray', linewidth=1)
            ax2.axvline(block, color='gray', linewidth=1)
    except:
        pass


    ### PLOT 3: STIMULUS DURATION

    ax3.clear()

    # STIMULUS DURATION PLOT (only plotted in stage 2)
    if stage == 2:
        hlines = [0.4, 0.3, 0.2, 0.1, 0]
        color = ds_c  # substage 1 --> reduce DS stim extension
        var = 'stim_dur_ds'
        variable=trials.stim_dur_ds
        # if substage == 2:  # substage 2 --> reduce DM stim extension
        #     color = dm_c
        #     var = 'stim_dur_dm'
        # elif substage == 3:  # substage 3 --> reduce DM stim extension
        #     color = dl_c
        #     var = 'stim_dur_dl'
        sns.lineplot(x=trials.TRIAL, y=trials[var], style=trials.trial_type_simple, markers=True, ax=ax3, color=color)  # marker='o'
        ax3.hlines(y=hlines, xmin=0, xmax=x_max, color=lines2_c,  linestyle=':', linewidth=1)
        ax3.set_ylabel('Stim extension (s)')
        ax3.set_xlim(-1, x_max)

    else:
        ax3.set_title('Empty plot in stages different from 2', y=1.0, pad=-20)

    # legend
    labels = list(ttypes_s)
    labels.append('All trials')
    colors = list(ttype_colors)
    colors.append('black')

    lines = [Line2D([0], [0], color=colors[i], marker='o', markersize=7, markerfacecolor=colors[i]) for i in
             range(len(colors))]
    ax3.legend(lines, labels, title='Trial type', fontsize=8, loc='center', bbox_to_anchor=(0.9, 0.9))


    ### PLOT 4: ACCURACY VS STIMULUS POSITION

    ax4.clear()
    ttype_palette = sns.set_palette(ttype_colors, n_colors=len(ttype_colors))  # palette creation
    sns.lineplot(x='x', y="correct_bool", data=first_resp_df, hue='trial_type_simple', marker='o', markersize=8, err_style="bars", ci=68, ax=ax4)

    ax4.hlines(y=chance_lines, xmin=l_edge, xmax=r_edge, color=lines_c, linestyle=':', linewidth= 1)
    ax4.fill_between(np.arange(l_edge, r_edge, 1), chance_p, 0, facecolor=lines2_c, alpha=0.3)
    ax4.set_xlabel('$Stimulus  \ position\ (x_{t})\ (mm)%$')
    ax4.set_ylabel('Accuracy (%)')
    ax4.set_ylim(0, 1.1)
    ax4.set_yticks(np.arange(0, 1.1, 0.1))
    ax4.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    ax4.get_legend().remove()


    ### PLOT 5: ERRORS VS STIMULUS POSITION
    ax5.clear()

    sns.lineplot(x='x', y="error_x", data=first_resp_df, hue='trial_type_simple', marker='o', markersize=8, err_style="bars", ci=68, ax=ax5)
    ax5.hlines(y=[-correct_th / 2, correct_th / 2], xmin=l_edge, xmax=r_edge, color=lines_c, linestyle=':', linewidth= 1 )

    ax5.set_xlabel('$Stimulus \ position\ (x_{t})\ (mm)%$')
    ax5.set_ylabel('Error (mm)')
    ax5.get_legend().remove()


    ### PLOT 6: RESPONSE COUNTS
    ax6.clear()
    bins_resp = np.linspace(int(min(x_positions) - correct_th/2), int(max(x_positions) + correct_th/2),  choices + 1)

    for ttype, ttype_df in trials.groupby('trial_type_simple'):
        ttype_color = ttype_df.ttype_colors.iloc[0]
        hist,bins = np.histogram(ttype_df.response_x, bins = bins_resp, density=True)
        bins_plt = bins[:-1] + (bins[1] - bins[0]) / 2
        sns.lineplot(x=bins_plt, y=hist, marker='o', markersize=8, err_style="bars", color=ttype_color)

    ax6.set_xlabel('$Responses\ (r_{t})\ (mm)$')
    ax6.set_ylabel('Counts')
    ax6.set_xlim(l_edge, r_edge)


def animate(i):
    try:
        data = pd.read_csv(settings.PATH, sep=';')
    except pd.errors.EmptyDataError:
        data = pd.DataFrame()

    if data.shape[0] > 0:
         try:
            # Lickteaching plot
            if data['VALUE'].str.contains('LickTeaching').any():
                trials, perc_valids = data_parse(data)
                lickteaching_plot(trials, perc_valids, settings.X_MAX)

            # Touchteaching plot
            elif data['VALUE'].str.contains('TouchTeaching').any():
                trials, perc_valids = data_parse(data)
                touchteaching_plot(data, trials, perc_valids, settings.X_MAX)

            # StageTraining plot
            elif data['VALUE'].str.contains('StageTraining').any():
                stagetraining_plot(data, settings.X_MAX)
         except:
           pass


fig = plt.figure()
ax1 = fig.add_subplot(4, 1, 1)
ax2 = fig.add_subplot(4, 1, 2)
ax3 = fig.add_subplot(4, 1, 3)
ax4 = fig.add_subplot(4, 3, 10)
ax5 = fig.add_subplot(4, 3, 11)
ax6 = fig.add_subplot(4, 3, 12)

ani = FuncAnimation(fig, animate, interval=2000)
plt.tight_layout()
sns.despine()
plt.show()
