# THIS IS A BACKUP FILE. DO NOT CHANGE IT.
# THE FIRST TIME THE APPLICATION IS LAUNCHED, A COPY OF THIS FILE IS CREATED IN THE USER DIRECTORY.
# ALWAYS MODIFY THE WORKING VERSION IN THE USER DIRECTORY.


import pkg_resources

DISTRIBUTION_DIRECTORY = pkg_resources.get_distribution('academy_rt_plots').location
PATH = '/home/setup5/academy/data/sessions/session.csv'
X_MAX = 70